[![pipeline status](https://gitlab.com/guided-explorations/aws/aws-cli-tools/badges/master/pipeline.svg)](https://gitlab.com/guided-explorations/aws/aws-cli-tools/-/commits/master)

## AWS CLI Tools

Amazon CLIs as individual containers or an all-in-one.

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/aws/kaniko-docker-build)

#### GitLab Public Registry Login

GitLab's public registry images require a GitLab login. Once you have a GitLab User id, [create a Personal Access Token (PAT)](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) to be used as your registry login password.

`docker login registry.gitlab.com -u your-gitlab-user-id -p your-gitlab-pat`

#### Use Credentials of Your Workstation

When using these containers on a workstation, the following code should work to map your local .aws/credentials file into the container. This same command should work from PowerShell and Bash as both have the $home variable.

`docker run -v $home/.aws:/root/.aws --entrypoint bash -it image_path_from_below`

#### All images contain
* OpenSSH (eb cli requires ssh-keygen - only in full openssh package)
* PowerShell 7 (55MB) for those who prefer that shell ;)
* AWS PowerShell CMDLets (30MB)
* AWS CLI v2 & cfn-lint
* envtpl - jinja template processing in the CLI to enable file templating any IaC Source files (https://github.com/andreasjansson/envtpl, https://github.com/mattrobenolt/envtpl)
* curl unzip jq tar gzip (not normally on amazon linux 2 container)

Start any image with "sh" or "pwsh".

#### All of the below in one (655MB)
image: registry.gitlab.com/guided-explorations/aws/aws-cli-tools/all-in-one

#### Shells and Frameworks (316MB)
image: registry.gitlab.com/guided-explorations/aws/aws-cli-tools/shells
* Nodejs & NPM
* Python2 & boto
* Python3
* PowerShell (55MB)
* AWS PowerShell CMDLets (30MB)
* AWS CLI v2 & cfn-lint
* Hashicorp Terraform

#### AWS CLI v2 (183MB)
image: registry.gitlab.com/guided-explorations/aws/aws-cli-tools/aws-cli
* PowerShell & AWS CMDLets
* AWS CLI v2 & cfn-lint
* envtpl
* Hashicorp Terraform

#### AWS EB CLI (222MB)
image: registry.gitlab.com/guided-explorations/aws/aws-cli-tools/aws-eb-cli
* PowerShell & AWS CMDLets
* AWS CLI v2 & cfn-lint
* envtpl
* AWS CLI v2
* aws cfn-lint
* aws eb CLI

#### AWS Container CLIs (350MB)
image: registry.gitlab.com/guided-explorations/aws/aws-cli-tools/aws-container-clis
* PowerShell & AWS CMDLets
* AWS CLI v2 & cfn-lint
* envtpl
* ecs-CLI
* eksctl
* kubectl
* aws iam-authenticator
* helm 2 (command named "helm2")
* helm 3 (command named "helm")
* Hashicorp Terraform

#### Serverless CLIs (602MB)
image: registry.gitlab.com/guided-explorations/aws/aws-cli-tools/aws-serverless-clis
* PowerShell & AWS CMDLets
* AWS CLI v2 & cfn-lint
* envtpl
* aws amplify CLI
* aws SAM CLI

#### AI Machine Learning (ML) CLIs (2000MB)
config guide: https://aws.amazon.com/blogs/machine-learning/building-an-interactive-and-scalable-ml-research-environment-using-aws-parallelcluster/
image: registry.gitlab.com/guided-explorations/aws/aws-cli-tools/aws-machine-learning-clis
* PowerShell & AWS CMDLets
* AWS CLI v2 & cfn-lint
* envtpl
* conda
* pcluster (as a conda environment)

#### AWS CDK CLI (769MB)
image: registry.gitlab.com/guided-explorations/aws/aws-cli-tools/aws-cdk-cli
* PowerShell & AWS CMDLets
* AWS CLI v2
* envtpl
* AWS CDK CLI with Nodejs and Python support

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-05-13

* **GitLab Version Released On**: 12.10

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

- **Tested On**: 
  - GitLab Docker-Executor Runner (GitLab.com Shared Runner) without enabling DinD Service.

## References and Featured In

* The concepts, principles and code for using Kaniko to build containers without docker privileged mode is covered here: https://gitlab.com/guided-explorations/containers/kaniko-docker-build

## Demonstrates These Design Requirements, Desirements and AntiPatterns

* **Containers for Build Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Build granular, functionally specialized containers with specific build dependencies for various CI CD needs.
* **Development Anti-Pattern:** The "all-in-one" container breaks the above pattern.
